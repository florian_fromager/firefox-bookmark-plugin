const errorContent = document.querySelector( "#error-content" );

function tabAction() {

    document.querySelector( "button" ).addEventListener( "click", e => {

        function save( tabs ) {

            browser.tabs.sendMessage( tabs[0].id, {

                command: "click",

            });

        }

        function reportError( error ) {

          console.error( `Could not save: ${ error }` );
          errorContent.innerHTML += `Could not save: ${ error }`;

        }

        browser.tabs.query( { active: true, currentWindow: true} )
        .then( save )
        .catch( reportError );

    });

}

function reportExecuteScriptError( error ) {

    document.querySelector( "#popup-content" ).classList.add( "hidden" );
    errorContent.classList.remove( "hidden" );
    errorContent.innerHTML += `Failed to execute the content script: ${ error.message }`;
    console.error( `Failed to execute the content script: ${ error.message }` );

}

browser.tabs.executeScript( { file: "/scripts.js" } )
.then( tabAction )
.catch( reportExecuteScriptError );
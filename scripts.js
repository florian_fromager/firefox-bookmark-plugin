( function() {

    /**
     * On vérifie que le script ne se lance qu'une seule fois.
     */
    if ( window.hasRun ) {

      return;

    }

    window.hasRun = true;

    /**
     * On écoute les messages envoyés par le script du popup.
     */
    browser.runtime.onMessage.addListener( message => {

        const JSONPath = browser.runtime.getURL( "data.json" );

        if ( message.command === "click" ) {

            let url = window.location.href;
                url = url !== null ? url : document.URL;
            
            console.log( `url: ${ url }` );

            fetch ( JSONPath, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json;charset=utf-8',
                },
            } )
            .then (
                response => {
                    return response.json();
                },
            )
            .then (
                data => console.log( data ),
            )
            .catch (
                error => {
                    console.error( error );
                }
            );


        }

    });

})();
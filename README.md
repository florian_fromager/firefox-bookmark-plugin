# Firefox bookmark plugin

A simple firefox plugin to save websites

## Goals

✔ Create a simple firefox plugin that extract page URL
* Insert page URLs as objects into a JSON
* Add tags and comments to this page
* Display it in a landing page
* Add a simple search form by name, tags and comments
* Add preferences to set custom URL for the JSON or the landing page